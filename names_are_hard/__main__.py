import gi
from gi.repository import Gtk
import os


class Handler:
    def on_destroy(self, window):
        Gtk.main_quit()


builder = Gtk.Builder.new_from_file(f"{os.path.dirname(__file__)}/app.ui")
handler = Handler()
builder.connect_signals(handler)
window: Gtk.Window = builder.get_object("main_window")
window.show()
Gtk.main()
